"""Validation functions for the various objects in our database
"""
import types

import utils


def id_(id_):
    """Ensure an ID looks sane-ish. Returns the id as an int
    """
    if not isinstance(id_, types.IntType):
        try:
            id_ = int(id_)
        except (TypeError, ValueError):
            raise utils.StoneretreatException('invalid id')
    return id_


def validate(obj, expect_id, expected_types, optional_types=None):
    """A generic validator that can check all our objects

    :param obj:  the object to be validated
    :param expect_id:  whether or not the object should have an id in it
    :param expected_types: a dict of key=>type listing the keys obj should have (other than id) and the type their value should be
    :param optional_types: like expected_types, but these items may be absent from the object or None
    :return: nothing - raises an exception if validation fails
    """
    expected_keys = set()
    for key, type_ in expected_types.iteritems():
        expected_keys.add(key)
        value = obj.get(key, None)
        if not isinstance(value, type_):
            raise utils.StoneretreatException('invalid %s: %s' % (key, value))

    if optional_types:
        for key, type_ in optional_types.iteritems():
            expected_keys.add(key)
            value = obj.get(key, None)
            if not isinstance(value, type_) and not isinstance(value, types.NoneType):
                raise utils.StoneretreatException('invalid %s: %s' % (key, value))

    # Make sure the set of keys we got is exactly what we expect
    extra_keys = set(obj.keys()) - expected_keys
    if extra_keys:
        raise utils.StoneretreatException('got unexpected parameters: %s' % (', '.join(extra_keys),))

    # Now make sure we have an id, if necessary
    if expect_id:
        id_(obj.get('id', None))
    elif 'id' in obj:
        raise utils.StoneretreatException('got unexpected id %(id)s' % obj)


def accommodation(accommodation, expect_id=False):
    """Ensure the name, adult_price, and child_price are all valid for an accommodation.
    If expect_id is True, accommodation must also have a valid-looking id member,
    otherwise it must not have an id.
    Returns nothing, raises an exception if validation fails
    """
    if not accommodation:
        raise utils.StoneretreatException('No accommodation supplied')

    expected_types = {'name': types.StringType,
                      'adult_price': types.IntType,
                      'child_price': types.IntType}

    validate(accommodation, expect_id, expected_types)


def person(person, expect_id=False):
    """Ensures a person object looks sane
    """
    if not person:
        raise utils.StoneretreatException('no person info provided')

    expected_types = {'name': types.StringType,
                      'accommodation': types.IntType,
                      'age_group': types.IntType,
                      'comped': types.BooleanType,
                      'deposit_paid': types.BooleanType,
                      'fully_paid': types.BooleanType}

    optional_types = {'group': types.IntType}

    validate(person, expect_id, expected_types, optional_types=optional_types)


def group(group, expect_id=False):
    """Ensures a group object looks sane
    """
    if not group:
        raise utils.StoneretreatException('no group info provided')

    expected_types = {'name': types.StringType}
    optional_types = {'members': types.ListType}

    validate(group, expect_id, expected_types, optional_types=optional_types)

    # Now make sure every element of members looks sane
    members = group.get('members', [])
    for m in members:
        if not isinstance(m, types.IntType):
            raise utils.StoneretreatException('invalid member id: %s' % (m,))


def charge(charge, expect_id=False):
    """Ensures a charge object looks sane
    """
    if not charge:
        raise utils.StoneretreatException('no charge info provided')

    expected_types = {'name': types.StringType,
                      'price': types.IntType,
                      'per_person': types.BooleanType,
                      'per_count': types.IntType}

    validate(charge, expect_id, expected_types)
