"""General utilities for the stoneretreat project
"""

import base64
import functools
import json
import os

from flask import g
from flask import redirect
from flask import request
from flask import session


class StoneretreatException(Exception):
    """An exception class we can use to hide tracebacks from the UI
    """
    pass


def user_pass_ok(username, password):
    """Check to see if username & password are allowed for authentication
    :param username: the username to check
    :param password: the password to check
    :return: True if the authentication is ok, False otherwise
    """
    # Since we're deploying somewhere like heroku/openstack, we
    # keep our password database in an environment variable. The
    # value of that variable is a base64-encoded json object with
    # usernames as keys and passwords as values.
    authstr = os.getenv('STONERETREAT_AUTH', '')
    g.log.debug('authstr = %s' % (authstr,))
    if not authstr:
        g.log.debug('no auth available')
        return False

    try:
        authjson = base64.b64decode(authstr)
    except Exception as e:
        g.log.debug('failed to decode auth: %s' % (str(e),))
        return False

    try:
        auth = json.loads(authjson)
    except Exception as e:
        g.log.debug('failed to json load auth: %s' % (str(e),))
        return False

    if username not in auth:
        g.log.debug('no username %s' % (username,))
        return False

    pw = auth[username]
    if password != pw:
        g.log.debug('passwords do not match')
        return False

    # Finally got to the point where the user is in our auth db, and the
    # password matches, so we can let them in.
    return True


def auth_ok():
    """Helper to ensure the user is properly authenticated
    """
    g.log.debug('auth_ok')

    # This is our usual route for authentication through the UI, which relies on cookie-based authentication instead
    # of HTTP basic auth.
    if 'auth_ok' in session:
        g.log.debug('already logged in - ok')
        return True
    # TODO - ensure sanity of auth_ok token in the session to prevent replay

    # Below here, we're checking for HTTP basic auth. This is used for CLI testing of the API
    g.log.debug('falling back to basic auth')
    if not request.authorization:
        # No auth header sent
        g.log.debug('no Authorization header')
        return False

    return user_pass_ok(request.authorization.username, request.authorization.password)


def authenticated(f):
    """A decorator to protect URIs with basic auth.
    """
    @functools.wraps(f)
    def decorated(*args, **kwargs):
        if not auth_ok():
            g.log.debug('auth not ok - go to login')
            return redirect('/login')
        return f(*args, **kwargs)
    return decorated
