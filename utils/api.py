"""Utilities for the API endpoints
"""
import json


def make_response(obj, status, message):
    """Create a json response object for the UI

    :param obj: the api-created object to return
    :param status: 'ok' or 'error'
    :param message: if status == 'error', an error message, otherwise ''
    """
    return json.dumps({
        'value': obj,
        'status': status,
        'message': message
    })


def make_success(obj):
    """Create a json response object for a successful operation

    :param obj: the object to send in the response
    """
    return make_response(obj, 'ok', '')


def make_error(message):
    """Create a json response object corresponding to an error

    :param message: the error message
    """
    return make_response({}, 'error', message)
