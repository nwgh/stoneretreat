"""API endpoints to manage groups in the stoneretreat database
"""


from flask import g, request

from api import blueprint as api

import utils
from utils.api import make_success, make_error
from utils.validators import group as validate
from utils.validators import id_ as validate_id


@api.route('/groups', methods=['GET'])
def list_groups():
    """Return a json list of all the groups in the database
    """
    try:
        rval = g.tx.groups.list_()
    except utils.StoneretreatException as e:
        return make_error(str(e))
    return make_success(rval)


@api.route('/group/<id_>', methods=['GET'])
def get_group(id_):
    """API endpoint to get a group from the database
    """
    try:
        id_ = validate_id(id_)
        group = g.tx.groups.get(id_)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(group)


@api.route('/group', methods=['PUT'])
def add_group():
    """API endpoint to add a group to the database
    """
    group = request.get_json(force=True, silent=True)
    try:
        validate(group, expect_id=False)
        group = g.tx.groups.new(group)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(group)


@api.route('/group/<id_>', methods=['POST'])
def update_group(id_):
    """API endpoint to modify a group's info in the database
    """
    group = request.get_json(force=True, silent=True)

    try:
        group['id'] = validate_id(id_)
        validate(group, expect_id=True)
        g.tx.groups.udpate(group)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(group)


@api.route('/group/<id_>', methods=['DELETE'])
def delete_group(id_):
    """API endpoint to remove a group from the database
    """
    try:
        id_ = validate_id(id_)
        g.tx.groups.delete(id_)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success({})
