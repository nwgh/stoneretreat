"""API endpoints to manage the years that the stone retreat has happened
"""


import time

from flask import g, request, session

from api import blueprint as api

import utils
from utils.api import make_success, make_error


@api.route('/years', methods=['GET'])
def list_years():
    """Return a json list of all the years
    """
    g.log.debug('list_years')
    try:
        rval = g.tx.years.list_()
    except utils.StoneretreatException as e:
        return make_error(str(e))
    return make_success(rval)


@api.route('/year/create_this_year', methods=['GET'])
def add_this_year():
    """Adds the current year to the database, and makes it the year that
    modifications will apply to
    """
    g.log.debug('add_this_year')
    this_year = time.localtime(time.time()).tm_year
    g.log.debug('year = %s' % (this_year,))
    try:
        g.tx.years.create(this_year)
    except utils.StoneretreatException as e:
        g.log.debug('failed to create year: %s' % (str(e),))
        return make_error(str(e))
    return make_success({'year': this_year})


@api.route('/year/<year>', methods=['GET'])
def get_year(year):
    """Get a year out of the database
    :return:
    """
    g.log.debug('get_year %s' % (year,))
    return make_error('Not implemented')


@api.route('/year/<year>', methods=['PUT'])
def add_year(year):
    """Adds an arbitrary year to the database, and makes it the year that
    modifications will apply to
    """
    g.log.debug('add_year %s' % (year,))
    return make_error('Not implemented')


@api.route('/year/<year>', methods=['DELETE'])
def delete_year(year):
    """Remove a year (and all its associated data) from the database.
    """
    g.log.debug('delete_year %s' % (year,))
    return make_error('Not implemented')


@api.route('/year/<year>', methods=['POST'])
def update_year(year):
    """Currently not used - nothing to do
    """
    g.log.debug('update_year %s' % (year,))
    return make_error('Not implemented')


@api.route('/year/set', methods=['POST'])
def set_year():
    """Sets the year to be modified in the database
    """
    g.log.debug('set_year')
    req = request.get_json(force=True, silent=True)
    g.log.debug('req = %s' % (req,))
    if not req:
        return make_error('Bad request')

    if 'year' not in req:
        return make_error('Missing year')

    try:
        # We don't strictly have to do this here, but nice to make sure it'll
        # work before shoving something potentially broken in the session
        g.tx.years.use(req['year'])
    except utils.StoneretreatException as e:
        g.log.debug('failed to use year: %s' % (str(e),))
        return make_error(str(e))

    session['year'] = req['year']

    return make_success({'year': req['year']})
