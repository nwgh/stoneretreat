"""API endpoints to manage people in the stoneretreat database
"""


from flask import g, request

from api import blueprint as api

import utils
from utils.api import make_success, make_error
from utils.validators import person as validate
from utils.validators import id_ as validate_id


@api.route('/people', methods=['GET'])
def list_people():
    """Return a json list of all the people in the database
    """
    try:
        rval = g.tx.people.list_()
    except utils.StoneretreatException as e:
        return make_error(str(e))
    return make_success(rval)


@api.route('/person/<id_>', methods=['GET'])
def get_person(id_):
    """API endpoint to get a person from the database
    """
    try:
        id_ = validate_id(id_)
        person = g.tx.people.get(id_)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(person)


@api.route('/person', methods=['PUT'])
def add_person():
    """API endpoint to add a person to the database
    """
    person = request.get_json(force=True, silent=True)
    try:
        validate(person, expect_id=False)
        person = g.tx.people.new(person)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(person)


@api.route('/person/<id_>', methods=['POST'])
def update_person(id_):
    """API endpoint to modify a person's info in the database
    """
    person = request.get_json(force=True, silent=True)

    try:
        person['id'] = validate_id(id_)
        validate(person, expect_id=True)
        g.tx.people.udpate(person)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(person)


@api.route('/person/<id_>', methods=['DELETE'])
def delete_person(id_):
    """API endpoint to remove a person from the database
    """
    try:
        id_ = validate_id(id_)
        g.tx.people.delete(id_)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success({})
