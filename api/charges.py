"""API endpoints to manage charges in the stoneretreat database
"""


from flask import g, request

from api import blueprint as api

import utils
from utils.api import make_success, make_error
from utils.validators import charge as validate
from utils.validators import id_ as validate_id


@api.route('/charges', methods=['GET'])
def list_charges():
    """Return a json list of all the charges in the database
    """
    try:
        rval = g.tx.charges.list_()
    except utils.StoneretreatException as e:
        return make_error(str(e))
    return make_success(rval)


@api.route('/charge/<id_>', methods=['GET'])
def get_charge(id_):
    """API endpoint to get a charge from the database
    """
    try:
        id_ = validate_id(id_)
        charge = g.tx.charges.get(id_)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(charge)


@api.route('/charge', methods=['PUT'])
def add_charge():
    """API endpoint to add a charge to the database
    """
    charge = request.get_json(force=True, silent=True)
    try:
        validate(charge, expect_id=False)
        charge = g.tx.charges.new(charge)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(charge)


@api.route('/charge/<id_>', methods=['POST'])
def update_charge(id_):
    """API endpoint to modify a charge's info in the database
    """
    charge = request.get_json(force=True, silent=True)

    try:
        charge['id'] = validate_id(id_)
        validate(charge, expect_id=True)
        g.tx.charges.udpate(charge)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(charge)


@api.route('/charge/<id_>', methods=['DELETE'])
def delete_charge(id_):
    """API endpoint to remove a charge from the database
    """
    try:
        id_ = validate_id(id_)
        g.tx.charges.delete(id_)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success({})
