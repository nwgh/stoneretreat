"""The API endpoints for the stoneretreat app

This is where most of the functionality lives.
"""


from flask import Blueprint

# The urls here all live under "/api"
blueprint = Blueprint('api', __name__, url_prefix='/api')

# The rest of these exist to make sure the API endpoints get registered
# with the top-level app when it adds our blueprint.
import accommodations
import charges
import groups
import people
import years
