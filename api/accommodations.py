"""API endpoints to manage accommodations in the stoneretreat database
"""


from flask import g, request

from api import blueprint as api

import utils
from utils.api import make_success, make_error
from utils.validators import accommodation as validate
from utils.validators import id_ as validate_id


@api.route('/accommodations', methods=['GET'])
def list_accommodations():
    """Get a list of all the accommodations for this year
    """
    try:
        rval = g.tx.accommodations.list_()
    except utils.StoneretreatException as e:
        return make_error(str(e))
    return make_success(rval)


@api.route('/accommodation/<id_>', methods=['GET'])
def get_accommodation(id_):
    """Get an accommodation from the database
    """
    try:
        id_ = validate_id(id_)
        accommodation = g.tx.accommodations.get(id_)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(accommodation)


@api.route('/accommodation', methods=['PUT'])
def add_accommodation():
    """Add an accommodation to the database
    """
    accommodation = request.get_json(force=True, silent=True)
    try:
        validate(accommodation, expect_id=False)
        accommodation = g.tx.accommodations.new(accommodation)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(accommodation)


@api.route('/accommodation/<id_>', methods=['POST'])
def update_accommodation(id_):
    """Update an accommodation in the database
    """
    accommodation = request.get_json(force=True, silent=True)
    if not accommodation:
        return make_error('No accommodation details provided')

    try:
        accommodation['id'] = validate_id(id_)
        validate(accommodation, expect_id=True)
        accommodation = g.tx.accommodations.update(accommodation)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success(accommodation)


@api.route('/accommodation/<id_>', methods=['DELETE'])
def delete_accommodation(id_):
    """Remove an accommodation from the database
    """
    try:
        id_ = validate_id(id_)
        g.tx.accommodations.delete(id_)
    except utils.StoneretreatException as e:
        return make_error(str(e))

    return make_success({})
