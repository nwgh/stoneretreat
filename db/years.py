"""Manage the years in the database
"""


import accommodations
import charges
import groups
import people
import utils


from psycopg2.extensions import adapt, register_adapter, AsIs


class SchemaName(object):
    """A wrapper class that we can adapt to get a proper label as a schema name in
    database queries.
    """
    def __init__(self, year):
        self.year = year


def adapt_schemaname(schema_name):
    """psycopg2 adapter to create schema names from a year

    :param schema_name: SchemaName object
    :return: a properly-escaped schema name
    """
    year = adapt(schema_name.year).getquoted()
    return AsIs('year_%s' % (year,))


register_adapter(SchemaName, adapt_schemaname)


def list_(db):
    """List all the years we have
    """
    db.execute("SELECT * FROM public.retreat_years")
    return db.dictfetchall()


def use(db, year):
    """Make the year passed in our currently-operational year on the database
    by modifying the postgresql search path.
    """
    db.execute("SELECT * FROM public.retreat_years WHERE year = %s", (year,))
    if not db.fetchall():
        raise utils.StoneretreatException('ERROR: year %s not configured' % (year,))
    db.execute("SET search_path TO %s", (SchemaName(year),))
    db.year = year


def create(db, year):
    """Add a new year to our list of years, create a schema for it, and make it active
    """
    db.execute("SELECT * FROM public.retreat_years WHERE year = %s", (year,))
    if db.fetchall():
        raise utils.StoneretreatException('ERROR: year %s already exists' % (year,))

    db.execute("INSERT INTO public.retreat_years VALUES (%s)", (year,))
    db.execute("CREATE SCHEMA %s", (SchemaName(year),))
    use(db, year)

    # Let the rest of the bits create their own tables
    accommodations.create(db)
    charges.create(db)
    people.create(db)
    groups.create(db)
