"""Utilities to interface with the database
"""


import functools
import logging
import psycopg2
import psycopg2.extensions


import people as __people_module
import groups as __groups_module
import accommodations as __accommodations_module
import charges as __charges_module
import years as __years_module


import utils


from dj_database_url import config as get_db_config


raw_db_config = get_db_config(
    env='OPENSHIFT_STONERETREAT_DB_URL',
    default='postgres://nwgh@localhost/stoneretreat')


submodules = {
    'people': __people_module,
    'groups': __groups_module,
    'accommodations': __accommodations_module,
    'charges': __charges_module,
    'years': __years_module
}


class ModuleProxy(dict):
    """Class that wraps our sub-modules to automagically provide a cursor to
    them when their public functions are called.
    """
    def __init__(self, cursor, module):
        super(ModuleProxy, self).__init__()
        self['cursor'] = cursor
        self['module'] = module

    def __getattribute__(self, item):
        attr = self['module'].__getattribute__(item)
        if not callable(attr):
            return attr
        return functools.partial(attr, self['cursor'])


class CustomCursor(psycopg2.extensions.cursor):
    """Our own implementation of psycopg2.extras.DictCursor

    We use this to provide magic around subsections of the database using
    ModuleProxy, as well as to ensure json serializability of results.
    """
    year = None
    # This is overridden by the web ui when running under the web, but we want this here just in case we're used
    # outside the ui
    log = logging.getLogger(__name__)

    def dictfetchall(self):
        """Return the results of the most recent statement as a list of dicts
        """
        columns = self.description
        column_names = [c.name for c in columns]
        rval = []
        for row in self.fetchall():
            rval.append(dict(zip(column_names, row)))
        return rval

    def dictfetchone(self):
        """Return the next row from the most recent statement as a dict
        """
        columns = self.description
        column_names = [c.name for c in columns]
        return dict(zip(column_names, self.fetchone()))

    def table_exists(self, tablename, schemaname=None):
        """Ensure a table exists

        :param tablename: the name of the table to search for
        :param schemaname: the name of the schema to search in (defaults to the current year)
        :return: boolean
        """
        if schemaname is None:
            schemaname = self.year
        self.execute("""SELECT tablename FROM pg_catalog.pg_tables WHERE
                        tablename = %s AND schemaname = %s""", (tablename, schemaname))
        return bool(self.fetchall())

    def ensure_initialized(self):
        """Make sure our basic schema is set up and ready to go
        """
        if not self.table_exists('retreat_years', schemaname='public'):
            self.execute("""CREATE TABLE public.retreat_years (
                year INTEGER PRIMARY KEY
            )""")

    def __getattribute__(self, item):
        """Wrapper to return ModuleProxy objects when we're really wanting a module
        """
        if item in submodules:
            return ModuleProxy(self, submodules[item])
        return super(CustomCursor, self).__getattribute__(item)

    def assert_year(self):
        """Raise an exception if the current year is not defined
        """
        if self.year is None:
            raise utils.StoneretreatException('INTERNAL ERROR: no year set')


def get_conn():
    """Get a connection to the database that provides our custom cursor when
    a cursor is requested.
    """
    # Parse the database configuration into something psycopg2 can handle
    dbconfig = {}
    if raw_db_config['NAME']:
        dbconfig['database'] = raw_db_config['NAME']
    if raw_db_config['USER']:
        dbconfig['user'] = raw_db_config['USER']
    if raw_db_config['PASSWORD']:
        dbconfig['password'] = raw_db_config['PASSWORD']
    if raw_db_config['HOST']:
        dbconfig['host'] = raw_db_config['HOST']
    if raw_db_config['PORT']:
        dbconfig['port'] = raw_db_config['PORT']
    connection = psycopg2.connect(**dbconfig)
    connection.cursor_factory = CustomCursor
    return connection
