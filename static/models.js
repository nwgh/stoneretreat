/* Generic function to parse the objects our API returns */
function parseApiObject(obj) {
    return obj.value;
}

var YearModel = Backbone.Model.extend({
    defaults: {
        year: null
    },

    parse: parseApiObject
});

var YearCollection = Backbone.Collection.extend({
    url: '/api/years',
    model: YearModel
});

var PersonModel = Backbone.Model.extend({
    defaults: {
        id: null,
        name: null,
        accommodation: null,
        age_group: null,
        comped: null,
        deposit_paid: null,
        fully_paid: null,
        group: null
    },

    parse: parseApiObject
});

var PersonCollection = Backbone.Collection.extend({
    url: '/api/people',
    model: PersonModel
});

var GroupModel = Backbone.Model.extend({
    defaults: {
        id: null,
        name: null,
        people: []
    },

    parse: parseApiObject
});

var GroupCollection = Backbone.Collection.exclusive({
    url: '/api/groups',
    model: GroupModel
});

var ChargeModel = Backbone.Model.extend({
    defaults: {
        id: null,
        name: null,
        price: null,
        per_person: null,
        per_count: null
    },

    parse: parseApiObject
});

var ChargeCollection = Backbone.Collection.extend({
    url: '/api/charges',
    model: ChargeModel
});

var AccommodationModel = Backbone.Model.extend({
    defaults: {
        id: null,
        name: null,
        adult_price: null,
        child_price: null
    },

    parse: parseApiObject
});

var AccommodationCollection = Backbone.Collection.extend({
    url: '/api/accommodations',
    model: AccommodationModel
});
