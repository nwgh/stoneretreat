"""The stone retreat application
"""
import os

from flask import flash
from flask import Flask
from flask import g
from flask import get_flashed_messages
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

import utils

import api
import db

app = Flask(__name__)

app.register_blueprint(api.blueprint)

app.secret_key = os.getenv('STONERETREAT_SECRET', os.getenv('OPENSHIFT_SECRET_TOKEN', 'for_testing_only'))

# Strip some of the crazy off our logging
app.debug_log_format = "%(levelname)s in %(module)s [%(pathname)s:%(lineno)d]: %(message)s"


@app.before_first_request
def before_first_request():
    """Hook to ensure our database is initialized the first time we are connected to
    """
    conn = db.get_conn()
    conn.cursor().ensure_initialized()
    conn.commit()
    conn.close()


@app.before_request
def before_request():
    """A hook to set up a database connection before
    we do anything, and make it globally available.
    """
    app.logger.debug('setup request')
    conn = db.get_conn()
    g.tx = conn.cursor()
    g.tx.log = g.log = app.logger
    year = session.get('year', None)
    g.log.debug('session[year] = %s' % (year,))
    if year is not None:
        try:
            g.tx.years.use(year)
        except Exception as e:
            # TODO: this may not be the right way to handle this
            g.log.debug('failed to use year %s: %s' % (year, str(e)))
            del session['year']


@app.teardown_request
def teardown_request(exception):
    """A hook to clean up our database once the
    request is returning.
    """
    g.log.debug('teardown request exception = %s' % (exception,))
    conn = g.tx.connection
    conn.commit()
    conn.close()


@app.route('/')
@utils.authenticated
def index():
    """The main landing page of the stoneretreat app
    """
    g.log.debug('index')
    return '''<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Move Along</title>
  </head>
  <body>
    Nothing to see here
  </body>
</html>'''


@app.route('/login')
def login():
    g.log.debug('login')
    message = get_flashed_messages()
    g.log.debug('message = %s' % (message,))
    return render_template('login.html', message=message)


@app.route('/logout')
def logout():
    g.log.debug('logout')
    if 'auth_ok' in session:
        g.log.debug('delete session[auth_ok]')
        del session['auth_ok']
    return redirect(url_for('login'))


@app.route('/authenticate', methods=['POST'])
def authenticate():
    g.log.debug('authenticate')
    try:
        username = request.form['username']
        password = request.form['password']
    except Exception as e:
        g.log.debug('missing username/password: %s' % (str(e),))
        flash('missing username/password')
        return redirect(url_for('login'))

    if not utils.user_pass_ok(username, password):
        g.log.debug('failed to validate user/pass %s/%s' % (username, password))
        flash('invalid username/password')
        return redirect(url_for('login'))

    g.log.debug('authentication successful')
    session['auth_ok'] = True
    return redirect(url_for('index'))


if __name__ == '__main__':
    # OK, now we can start serving stuff
    port = int(os.getenv('PORT', os.getenv('OPENSHIFT_PYTHON_PORT', 5000)))
    app.run(host='0.0.0.0', port=port, debug=bool(os.getenv('STONERETREAT_TESTING', False)))
